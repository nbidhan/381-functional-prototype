[33mcommit b452cc6c633bd3a6a6df1e4c103796bb79d003e6[m
Author: Steve Tene <stvtene@gmail.com>
Date:   Wed May 6 09:28:55 2015 +0930

    Add social login

[33mcommit ce24dbc401c30dff73a45948f66b31b373411695[m
Author: Bidhan Neupane <neupanebidhan@gmail.com>
Date:   Mon May 4 19:47:05 2015 +0930

    Fix model links

[33mcommit 29023248cc44b48ded94164ad2d54d778c7d89cd[m
Merge: 440bb2b 2197d26
Author: Bidhan Neupane <neupanebidhan@gmail.com>
Date:   Mon May 4 19:16:19 2015 +0930

    Merge changes to both buttons in single-video page

[33mcommit 2197d26d8c1a09e05c28d92b0054fc8bd0f3abdb[m
Author: Steve Tene <stvtene@gmail.com>
Date:   Mon May 4 19:13:33 2015 +0930

    Add single video rating module

[33mcommit 440bb2b6453cc3e421691aabb2407e41b4e28f9f[m
Author: Bidhan Neupane <neupanebidhan@gmail.com>
Date:   Mon May 4 19:12:23 2015 +0930

    Add popup for add to workout

[33mcommit 0896ddca5c5381f47a61ac8a1f7cd45e1722c504[m
Author: Bidhan Neupane <neupanebidhan@gmail.com>
Date:   Sun May 3 17:18:36 2015 +0930

    Remove transition on tab navigation

[33mcommit 68b8d8f7ddacd306f7bc4bd4428f9f43ddd56a5c[m
Author: Bidhan Neupane <neupanebidhan@gmail.com>
Date:   Wed Apr 22 09:13:55 2015 +0930

    Inital prototype
